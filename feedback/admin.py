from django.contrib import admin
from models import Channel


class ChannelAdmin (admin.ModelAdmin):
	list_display = ('title_en', 'support_email')


admin.site.register(Channel, ChannelAdmin)
