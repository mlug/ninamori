# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from models import CommentsGroup
from ninamori.utils import cache_delete


def drop_cache(modeladmin, request, queryset):
	for comments_group in queryset:
		cache_delete('comments_' + comments_group.alias)

drop_cache.short_description = "Drop cache for selected groups"


class CommentsGroupAdmin (admin.ModelAdmin):
	list_display = ('alias', 'mode', 'premoderation_mode', 'sorting_mode')
	ordering = ['alias']
	fieldsets = [
		(None, {'fields': ['mode', 'premoderation_mode', 'sorting_mode']}),
	]
	actions = [drop_cache]

	def has_add_permission(self, request):
		return False


admin.site.register(CommentsGroup, CommentsGroupAdmin)
