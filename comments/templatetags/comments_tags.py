from django import template
from django.template.loader import render_to_string
from datetime import datetime, timedelta
import httpagentparser

register = template.Library()


@register.simple_tag
def get_user_device_icon (agent):
	detect = httpagentparser.detect(agent)
	platform = detect['platform']['name']
	path = '/static/img/device_icons/'
	if platform == 'Linux':
		return path + 'linux.png'
	if platform == 'BlackBerry':
		return path + 'blackberry.png'
	if platform == 'Windows':
		return path + 'windows.png'
	if platform == 'iOS':
		return path + 'apple.png'
	if platform == 'Mac OS':
		return path + 'apple.png'
	if platform == ' ChromeOS':
		return path + 'chrome.png'
	if platform == 'Android':
		return path + 'android.png'
	if platform == 'Nokia S40':
		return path + 'nokia.png'
	if platform == 'Symbian':
		return path + 'nokia.png'
	if platform == 'PlayStation':
		return path + 'playstation.png'
	return path + 'unknown.png'


@register.simple_tag
def edit_button (time, uuid, email_updates, *args, **kwargs):
	# TODO properly use timezone (sadly, this require python3)
	if datetime.utcnow() - time > timedelta(hours=1):
		return ''
	return render_to_string('comments/edit_button.html', {'uuid': uuid, 'email_updates': email_updates})
