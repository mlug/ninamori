# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import transaction
from django.middleware.csrf import get_token
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from odict import SequenceOrderedDict
from forms import comment_form, edit_form
from models import Comment, CommentsGroup, Commentator
from ninamori.utils import get_name, cache_add, cache_get, cache_delete
from ninamori.templatetags.global_tags import minify_include_js
from urllib import quote_plus

cache_settings = settings.CACHES['default']
CACHE_DATETIME_FORMAT = '%Y %m %d %H %M'
COMMENTS_TREE_LEVEL_LIMIT = 10


def add_comment (user, comments_group, parent, content, ip, agent, language, email_updates):
	parent_approved = True if not parent or parent.approved else False
	approved = (comments_group.premoderation_mode == 'off' or comments_group.premoderation_mode == 'one' and user.has_perm('accounts.comments_approved')) and parent_approved
	email = user.email if user.is_authenticated() else None
	if email:
		commentator = Commentator.objects.get_or_create(email=email)[0]
		commentator.language = language
		commentator.save()
	comment = Comment.objects.create(
		approved=approved,
		parent=parent,
		content=content,
		email=email,
		email_updates=email_updates,
		user_ip=ip,
		user_agent=agent,
		user_language=language,
	)
	update_group_subscriptions(comment, comments_group)
	if comments_group.mode == 'tree' and parent:
		update_tree_commentators_subscriptions(comment, comments_group)
		# child comments will be inverted in get_comments retrieving process thus they added in reverse
		if comments_group.sorting_mode == 'top':
			parent.childs.append(comment.uuid)
		if comments_group.sorting_mode == 'bot':
			parent.childs.insert(0, comment.uuid)
		parent.save()
	else:
		if comments_group.sorting_mode == 'top':
			comments_group.comments.insert(0, str(comment.uuid))
		if comments_group.sorting_mode == 'bot':
			comments_group.comments.append(str(comment.uuid))
		comments_group.save()

	cache_delete('comments_' + comments_group.alias)
	return comment


def update_tree_commentators_subscriptions (comment, comments_group):
	op_email = comment.email
	op_comment = comment
	commentators_emails = []

	while comment.parent:
		email = comment.parent.email
		update = comment.parent.email_updates
		if email and update and (email not in commentators_emails) and (email != op_email):
			commentators_emails.append(email)
		comment = comment.parent

	update_commentators_subscriptions(commentators_emails, op_comment, comments_group)


def update_group_subscriptions (comment, comments_group):
	op_email = comment.email
	op_comment = comment
	commentators_emails = []

	for watcher in comments_group.watchers.all():
		if watcher.email != op_email:
			commentators_emails.append(watcher.email)

	update_commentators_subscriptions(commentators_emails, op_comment, comments_group)


def update_commentators_subscriptions (commentators_emails, comment, comments_group):
	with transaction.atomic():
		commentators = Commentator.objects.select_for_update().filter(email__in=commentators_emails)
		for commentator in commentators:
			comment_tuple = (str(comment.uuid), comments_group.alias)
			if not comment_tuple in commentator.notification_queue:
				commentator.notification_queue.append(comment_tuple)
				commentator.save()


def edit_comment (comment, comments_group, content, email_updates):
	comment.content = content
	comment.email_updates = email_updates
	comment.save()
	cache_delete('comments_' + comments_group.alias)
	return True


def get_cached_comments (comments_group):
	try:
		comments = cache_get('comments_' + comments_group.alias)
		assert(comments)
	except AssertionError:
		comments = get_comments(comments_group).items()
		cache_add('comments_' + comments_group.alias, comments, cache_settings['TIMEOUT'])
	return comments


def get_packed_comment_level (comment):
		pl = comment[1][4]
		level = pl if pl < COMMENTS_TREE_LEVEL_LIMIT else COMMENTS_TREE_LEVEL_LIMIT
		return level


def packed_branch_status (request, comment):
	return request.session.get('comments_branch_'+str(comment[0])+'unfolded') or request.COOKIES.get(quote_plus('comments_branch_'+str(comment[0]))+'unfolded')


def get_data_for_top_level_comments (request, comments, unfolded):
	result = []
	for i in xrange(0, len(comments)):
		level = get_packed_comment_level(comments[i])
		if level > 0:
			for j in xrange(i, -1, -1):
				if comments[j][1][4] == 0:
					branch_unfolded = packed_branch_status(request, comments[j])
					break
		else:
			branch_unfolded = packed_branch_status(request, comments[i])

		if unfolded or branch_unfolded or level == 0:
			result.append(level_comment(comments[i], level, branch_unfolded))

	return result


def get_top_level_comments_html (request):
	alias = request.path
	user = request.user
	moderator = user.has_perm('accounts.comments_moderate') or user.is_staff
	can_delete = user.has_perm('accounts.comments_delete') or user.is_staff

	js = minify_include_js('comments/comments_form.js', {
		'moderate_url': reverse('comments.views.moderate'),
		'watch_url': reverse('comments.views.watch'),
		'watch_group_url': reverse('comments.views.watch_group'),
		'unfold_url': reverse('comments.views.unfold_ajax'),
		'request_branch_url': reverse('comments.views.ajax_branch'),
		'moderator': moderator,
		'can_delete': can_delete,
	})
	if alias.find('scripts.js') != -1:
		return ('', js)

	comments_group = CommentsGroup.objects.get_or_create(alias=request.path)[0]
	comments = get_cached_comments(comments_group)
	unfolded = True if request.COOKIES.get(quote_plus('comments_'+alias+'unfolded')) or request.session.get('comments_'+alias+'unfolded') else False
	comments_data = get_data_for_top_level_comments(request, comments, unfolded)
	user_email = user.email if user.is_authenticated() else None
	user_email_valid = user.userprofile.validation_email_valid if user.is_authenticated() else False
	group_watched = True if comments_group.watchers.filter(email=user_email) else False
	show_agent_icons = user.is_authenticated
	mode = comments_group.mode
	premoderation_mode = comments_group.premoderation_mode
	c_form = comment_form(initial={'comments_group_alias': request.path})
	e_form = edit_form(initial={'comments_group_alias': request.path})

	html = render_to_string('comments/comments.html', {
		'comment_url': reverse('comments.views.comment'),
		'edit_url': reverse('comments.views.edit'),
		'unfold_url': reverse('comments.views.unfold'),
		'unfolded': unfolded,
		'alias': request.path,
		'comments': comments_data,
		'csrf_token': get_token(request),
		'comment_form': c_form,
		'edit_form': e_form,
		'can_delete': can_delete,
		'show_agent_icons': show_agent_icons,
		'show_deleted': (request.GET.get('show_deleted') and can_delete) or False,
		'group_data': {'mode': mode, 'moderator': moderator, 'COLORS': settings.THEME_COLORS},
		'group_watched': group_watched,
		'premoderation_mode': premoderation_mode,
		'user_comments': request.session.get('comments_own') or [],
		'user_email': user_email,
		'user_email_validated_or_anonymous': user_email and user_email_valid or not user_email and not user_email_valid,
	})
	return (html, js)


def coment_is_related_to_branch (comment,comments_group):
	c = comment
	recursion = 0
	while True:
		if str(c.uuid) in comments_group.comments:
			return True
		recursion += 1
		if not c.parent or recursion > 3:  # Can this even be posible? It´s not like there is non-root foldable branches
			return False
		c = c.parent


def get_comments_branch_html (comments_group, comment, user_email, user_email_valid, request):
	if not comment or not comments_group or not coment_is_related_to_branch(comment, comments_group):
		return render(request, '403.html', {'message': _('You shall not pass')}, status=400)

	comments = get_cached_comments(comments_group)
	result = []
	for i in xrange(0, len(comments)):
		if comments[i][0] == comment.uuid:
			j=i  # there should always be j because get_cached_comments should always return comments as long as coment_is_related_to_branch
			break
	for i in xrange (j+1, len(comments)):
		level = get_packed_comment_level(comments[i])
		if level == 0:
			break
		result.append(level_comment(comments[i], level, False))


	user = request.user
	c_form = comment_form(initial={'comments_group_alias': request.path})
	e_form = edit_form(initial={'comments_group_alias': request.path})
	moderator = user.has_perm('accounts.comments_moderate') or user.is_staff
	can_delete = user.has_perm('accounts.comments_delete') or user.is_staff
	show_agent_icons = user.is_authenticated
	mode = comments_group.mode
	premoderation_mode = comments_group.premoderation_mode

	return render(request, 'comments/branch.html', {
		'branch_uuid': str(comment.uuid),
		'comment_url': reverse('comments.views.comment'),
		'edit_url': reverse('comments.views.edit'),
		'unfold_url': reverse('comments.views.unfold'),
		'unfolded': False,
		'alias': comments_group.alias,
		'comments': result,
		'csrf_token': get_token(request),
		'comment_form': c_form,
		'edit_form': e_form,
		'can_delete': can_delete,
		'show_agent_icons': show_agent_icons,
		'show_deleted': (request.GET.get('show_deleted') and can_delete) or False,
		'group_data': {'mode': mode, 'moderator': moderator, 'COLORS': settings.THEME_COLORS},
		'group_watched': False,
		'premoderation_mode': premoderation_mode,
		'user_comments': request.session.get('comments_own') or [],
		'user_email': user_email,
		'user_email_validated_or_anonymous': user_email and user_email_valid or not user_email and not user_email_valid,
	})


# Childs is second item in comment tuple
def untuple_childs (t):
	return (
		t[0],
		True if t[1] else False,
		t[2], t[3], t[4], t[5], t[6], t[7], t[8], t[9], t[10], t[11],
	)

def level_comment (comment, level, branch_unfolded):
	cid = comment[0]
	c = comment[1]
	return (
		str(cid),
		c[0], c[1], c[2], c[3],
		level, 24-level,
		datetime.strptime(c[5], CACHE_DATETIME_FORMAT),
		c[6], c[7], c[8], c[9], c[10], c[11],
		branch_unfolded,
	)

def get_comments (comments_group):
	ordered_comments = SequenceOrderedDict()
	level = 0
	for comment_uuid in comments_group.comments:
		comment = Comment.objects.get(uuid=comment_uuid)
		ordered_comments[comment.uuid] = comment.tupleize_comment(level)

	while group_have_untreated_childs(ordered_comments):
		level += 1
		for ordered_comment in ordered_comments.items():
			index = ordered_comments.index(ordered_comment[0])
			if type(ordered_comment[1][1]) == list:
				for child_uuid in ordered_comment[1][1]:
					comment = Comment.objects.get(uuid=child_uuid)
					content = comment.tupleize_comment(level)
					ordered_comments.insert(index+1, comment.uuid, content)
			ordered_comments[ordered_comment[0]] = untuple_childs(ordered_comment[1])
	return ordered_comments


def group_have_untreated_childs (group):
	for item in group.items():
		if type(item[1][1]) == list and len(item[1][1]) and type(item[1][1][0]) == unicode:
			return True
	return False
