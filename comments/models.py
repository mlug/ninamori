# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from ninamori.utils import get_name
from jsonfield import JSONField
import datetime
import uuid

CACHE_DATETIME_FORMAT = '%Y %m %d %H %M'


class Comment (models.Model):
	uuid               = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	approved           = models.BooleanField(default=False)
	deleted            = models.BooleanField(default=False)
	childs             = JSONField(default=[])
	parent             = models.ForeignKey("Comment", null=True)
	content            = models.TextField()
	email              = models.EmailField(null=True)
	email_updates      = models.BooleanField(default=True)
	date_added         = models.DateTimeField(auto_now_add=True)
	user_ip            = models.CharField(max_length=46)
	user_agent         = models.CharField(max_length=4096)
	user_language      = models.CharField(max_length=4)

	def get_commentator_name (self):
		if not self.email:
			return 'Anonymous'
		try:
			user = User.objects.get(email=self.email)
			return get_name(user)
		except User.DoesNotExist:
			return 'Unknown'

	def tupleize_comment (self, level=0):
		return (
			self.approved,
			self.childs,
			self.content,
			self.email,
			level,
			self.date_added.strftime(CACHE_DATETIME_FORMAT),
			self.user_ip,
			self.user_agent,
			self.user_language,
			self.deleted,
			self.email_updates,
			self.get_commentator_name(),
		)


class Commentator (models.Model):
	email              = models.EmailField(primary_key=True)
	language           = models.CharField(max_length=4, choices=(("ru", "Russian"), ("en", "English")), default="ru")
	notification_queue = JSONField(default=[])


class CommentsGroup (models.Model):
	alias              = models.CharField(primary_key=True, max_length=254, unique=True, null=False, editable=False)
	allow_anonymous    = models.BooleanField(default=False)
	mode               = models.CharField(max_length=6, choices=(
		('tree', 'Tree'),
		('line', 'Linear'),
	), default='tree')
	premoderation_mode = models.CharField(max_length=5, choices=(
		('off', 'Off'),
		('one', 'Premoderation until user have first approved comment'),
		('evr', 'Premoderation for every comment'),
		('dis', 'Commentary disabled'),
	), default='evr')
	sorting_mode       = models.CharField(max_length=5, choices=(
		('top', 'New comments goes top'),
		('bot', 'New comments goes bottom'),
	), default='top')
	comments           = JSONField(default=[])
	watchers           = models.ManyToManyField(Commentator, db_table="comments_group_watchers", blank=True)
