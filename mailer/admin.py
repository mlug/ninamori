from django import forms
from django.contrib import admin
from django.contrib.admin.helpers import ActionForm
from django.http import HttpResponseRedirect
from ninamori.utils import validate_email
from .models import Subscriber, List, Message, Token
from .management.commands.sendmessages import sendmail


class SubscriberAdmin (admin.ModelAdmin):
	list_display = ('email', 'date_confirmed', 'date_unsubscribed', 'config_language')
	ordering = ['-date_subscribed']


class ListAdmin (admin.ModelAdmin):
	fieldsets = [
		(None,           {'fields': ['name', 'public', 'default', 'description_en', 'description_ru']}),
#		('Extra tuning', {'fields': ['user', 'validation_email_sent'], 'classes': ['collapse']}),
	]
	list_display = ('name', 'public', 'default')


class MessageActionForm (ActionForm):
	email = forms.EmailField()


def ready_messages (modeladmin, request, queryset):
	queryset.update(ready=True)
	ready_messages.short_description = 'Ready selected messages'


def test_messages (modeladmin, request, queryset):
	email = request.POST.get('email')
	if validate_email(email):
		for message in queryset:
			sendmail(Subscriber(email=email, name='Tester', config_access_token='test_text_en', config_send_html=False, config_language='en'), message)
			sendmail(Subscriber(email=email, name='Tester', config_access_token='test_html_en', config_send_html=True, config_language='en'), message)
			sendmail(Subscriber(email=email, name='Tester', config_access_token='test_text_ru', config_send_html=False, config_language='ru'), message)
			sendmail(Subscriber(email=email, name='Tester', config_access_token='test_html_ru', config_send_html=True, config_language='ru'), message)
test_messages.short_description = 'Test selected messages'


class MessageAdmin (admin.ModelAdmin):
	def changelist_view(self, request, extra_context=None):
		if not request.META['QUERY_STRING'] and \
		   not request.META.get('HTTP_REFERER', '').startswith(request.build_absolute_uri()):
			return HttpResponseRedirect(request.path + "?archived__exact=0")
		return super(MessageAdmin,self).changelist_view(request, extra_context=extra_context)

	action_form = MessageActionForm
	fieldsets = [
		(None,           {'fields': ['list', 'urgent', 'title', 'title_ru', 'title_en']}),
		('Message',      {'fields': ['message_text_ru', 'message_html_ru', 'message_text_en', 'message_html_en'], 'classes': ['wide']}),
	]
	list_filter = (
		('ready', admin.BooleanFieldListFilter),
		('archived', admin.BooleanFieldListFilter),
	)
	list_display = ('title', 'list', 'urgent', 'date_created', 'ready', 'archived')
	list_editable = ('urgent', 'ready')
	ordering = ['-date_created']
	actions = [ready_messages, test_messages]


class TokenAdmin (admin.ModelAdmin):
	readonly_fields = ('uuid', 'purpose')
	list_display = ('uuid', 'list', 'purpose')
	list_display_links = None
	search_fields = ['purpose']


admin.site.register(Subscriber, SubscriberAdmin)
admin.site.register(List, ListAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Token, TokenAdmin)
