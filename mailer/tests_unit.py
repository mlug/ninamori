# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User, AnonymousUser, Permission
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest
from django.test import TestCase
from management.commands import sendmessages
from models import Subscriber, List, Message, Token, HTML
import views, utils
from ninamori.utils import email_access_validate


class MailerTests (TestCase):
	def setUp (self):
		self.session = SessionMiddleware()

	def test_models_subscriber (self):
		subscriber = Subscriber.objects.create(
			email = 'm@e.ow',
			config_access_token = 'meow',
		)
		subscriber.name = 'meow'
		subscriber.list_of_lists = '|meow|'
		subscriber.save()
		subscriber = Subscriber.objects.get(email='m@e.ow')
		self.assertEqual(subscriber.name, 'meow')
		self.assertEqual(subscriber.list_of_lists, '|meow|')
		self.assertEqual(subscriber.__unicode__(), 'm@e.ow')

	def test_models_list (self):
		l = List.objects.create(
			name='meow',
			description_en='meow',
			description_ru='мяу',
		)
		uuid = l.uuid
		l = List.objects.get(uuid=uuid)
		self.assertEqual(l.name, 'meow')
		self.assertEqual(l.__unicode__(), 'meow')
		self.assertEqual(l.localized_description('en'), 'meow')
		self.assertEqual(l.localized_description('ru'), 'мяу')
		l.description_en = 'hehehe'
		l.description_ru = 'hehehe'
		l.save()
		self.assertEqual(l.localized_description('xxx'), 'hehehe')

	def test_models_message (self):
		l = List.objects.create(name='meow')
		message = Message.objects.create(list=l)
		message.title = 'meow'
		message.title_en = 'Meowing message'
		message.title_ru = 'Мяу'
		message.save()
		message = Message.objects.get(title='meow')
		self.assertEqual(message.title_en, 'Meowing message')
		self.assertEqual(message.title_ru, 'Мяу')
		self.assertEqual(message.__unicode__(), message.list.name + ' | ' + message.title)

	def test_models_localizers_return_list_description (self):
		l = List.objects.create(name='meow')
		l.description_ru = 'Мяу'
		l.description_en = 'meow'
		uuid = l.uuid
		l.save()
		l = List.objects.get(uuid=uuid)
		self.assertEqual(l.localized_description('en'), 'meow')
		self.assertEqual(l.localized_description('ru'), 'Мяу')

	def test_models_localizers_return_available_message_title (self):
		l = List.objects.create(name='meow')

		message = Message.objects.create(list=l)
		message.title = 'meow'
		message.title_en = 'Meowing message'
		message.title_ru = ''
		message.save()
		message = Message.objects.get(title='meow')
		self.assertEqual(message.localized_title('en'), 'Meowing message')
		self.assertEqual(message.localized_title('ru'), 'Meowing message')

		message = Message.objects.create(list=l)
		message.title = 'meow2'
		message.title_en = ''
		message.title_ru = 'Мяу'
		message.save()
		message = Message.objects.get(title='meow2')
		self.assertEqual(message.localized_title('en'), 'Мяу')
		self.assertEqual(message.localized_title('ru'), 'Мяу')
		message.title_en = ''
		message.title_ru = ''
		message.save()
		self.assertEqual(message.localized_title('en'), '')
		self.assertEqual(message.localized_title('ru'), '')

	def test_models_localizers_return_available_message_text (self):
		l = List.objects.create(name='meow')

		message = Message.objects.create(list=l)
		message.title = 'meow1'
		message.message_text_en = 'Meowing message'
		message.message_text_ru = ''
		message.message_html_en = HTML
		message.message_html_ru = HTML
		message.save()
		message = Message.objects.get(title='meow1')
		self.assertEqual(message.localized_message_text('en'), 'Meowing message')
		self.assertEqual(message.localized_message_text('ru'), 'Meowing message')
		self.assertEqual(message.localized_message_html('en'), HTML)
		self.assertEqual(message.localized_message_html('ru'), HTML)
		message.message_text_en = ''
		message.save()
		self.assertEqual(message.localized_message_text('en'), '')
		self.assertEqual(message.localized_message_text('ru'), '')

		message = Message.objects.create(list=l)
		message.title = 'meow2'
		message.message_text_en = ''
		message.message_text_ru = 'Мяу'
		message.message_html_en = HTML
		message.message_html_ru = HTML
		message.save()
		message = Message.objects.get(title='meow2')
		self.assertEqual(message.localized_message_text('en'), 'Мяу')
		self.assertEqual(message.localized_message_text('ru'), 'Мяу')
		self.assertEqual(message.localized_message_html('en'), HTML)
		self.assertEqual(message.localized_message_html('ru'), HTML)

		message = Message.objects.create(list=l)
		message.title = 'meow3'
		message.message_text_en = 'Meowing message'
		message.message_text_ru = ''
		message.message_html_en = 'meow'
		message.message_html_ru = HTML
		message.save()
		message = Message.objects.get(title='meow3')
		self.assertEqual(message.localized_message_text('en'), 'Meowing message')
		self.assertEqual(message.localized_message_text('ru'), 'Meowing message')
		self.assertEqual(message.localized_message_html('en'), 'meow')
		self.assertEqual(message.localized_message_html('ru'), 'meow')

		message = Message.objects.create(list=l)
		message.title = 'meow4'
		message.message_text_en = 'Meowing message'
		message.message_text_ru = ''
		message.message_html_en = HTML
		message.message_html_ru = 'Мяу'
		message.save()
		message = Message.objects.get(title='meow4')
		self.assertEqual(message.localized_message_text('en'), 'Meowing message')
		self.assertEqual(message.localized_message_text('ru'), 'Meowing message')
		self.assertEqual(message.localized_message_html('en'), HTML)
		self.assertEqual(message.localized_message_html('ru'), 'Мяу')

	def test_models_token (self):
		l = List.objects.create(name='meow')
		token = Token.objects.create(list=l)
		uuid = token.uuid
		token = Token.objects.get(uuid=uuid)
		self.assertEqual(token.list.name, 'meow')
		self.assertEqual(token.__unicode__(), str(uuid))

	def test_views_index (self):
		user = User.objects.create_user(username='meow1', email='1m@e.ow', password='purr')
		email_access_validate('1m@e.ow')
		user.userprofile.refresh_from_db()
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		request.user = AnonymousUser()
		response = views.index(request)
		self.assertIn(b'<form ', response.content)
		request.GET['bad_email'] = 'yes'
		request.LANGUAGE_CODE = 'en'
		response = views.index(request)
		self.assertIn(b'<div class="p-g">', response.content)
		request.user = user
		response = views.index(request)
		self.assertIn(b'1m@e.ow', response.content)

	def test_views_get_subscriber_or_false (self):
		subscriber = Subscriber.objects.create(
			email = 'm@e.ow',
			config_access_token = 'meow',
		)
		request = HttpRequest()
		request.POST['token'] = 'meow'
		self.assertEqual(views.get_subscriber_or_false(request), subscriber)
		request.POST['token'] = 'purr'
		self.assertFalse(views.get_subscriber_or_false(request))

	def test_views_generate_tokens (self):
		request = HttpRequest()
		request.user = User.objects.create(email='meow@meow.meow')
		request.LANGUAGE_CODE = 'en'
		self.session.process_request(request)
		response = views.generate_tokens(request)
		l = List.objects.create(name='meow')
		self.assertEqual(response.status_code, 405)  # POST method required
		request.method = 'POST'
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 403)  # user do not have permission
		request.user.user_permissions.add(Permission.objects.get(codename='mailer_generate_tokens'))
		request.user = User.objects.get(email='meow@meow.meow')
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 400)  # Incorrect token count
		request.POST = {'token_count': '1'}
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 403)  # should validate email
		email_access_validate('meow@meow.meow')
		request.user.userprofile.refresh_from_db()
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 400)  # Wrong UUID
		request.POST = {'token_count': '1', 'uuid': str(l.uuid), 'qr': '-1'}
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 400)  # Incorrect qr per page value
		request.POST = {'token_count': '1', 'uuid': str(l.uuid), 'qr': '24'}
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 302)  # generated tokens
		response = views.generate_tokens(request)
		self.assertEqual(response.status_code, 302)  # Try again later

# other views tests is not viable here and done in tests_func

	def test_utils_get_list_of_lists (self):
		list1 = List.objects.create(name='meow1', public=True)
		list2 = List.objects.create(name='meow2', public=True)
		list3 = List.objects.create(name='meow3', public=True)
		self.assertIn((list1.name, list1.uuid, list1.default), utils.get_list_of_lists())
		self.assertIn((list2.name, list2.uuid, list2.default), utils.get_list_of_lists())
		self.assertIn((list3.name, list3.uuid, list3.default), utils.get_list_of_lists())

	def test_utls_get_subsrcibe_form_html (self):
		request = HttpRequest()
		request.LANGUAGE_CODE = 'en'
		request.META = {'CSRF_COOKIE': 'meow'}
		result = utils.get_subsrcibe_form_html(request)
		self.assertIn('<form ', result)

	def test_utils_send_confirmation_email (self):
		subscriber = Subscriber.objects.create(
			email = 'm@e.ow',
			config_access_token = 'meow',
		)
		self.assertTrue(utils.send_confirmation_email(subscriber, resubscribe=False))
		self.assertTrue(utils.send_confirmation_email(subscriber, resubscribe=True))

	def test_utils_send_invitation_email (self):
		subscriber = Subscriber.objects.create(
			email = 'm@e.ow',
			config_access_token = 'meow',
		)
		l = List.objects.create(name='meow')
		token = Token.objects.create(list=l)
		uuid = token.uuid
		self.assertTrue(utils.send_invitation_email(subscriber, token))
		self.assertTrue(utils.send_invitation_email(subscriber, token, new_subscriber=True))

	def test_utils_send_new_token (self):
		subscriber = Subscriber.objects.create(
			email = 'm@e.ow',
			config_access_token = 'meow',
		)
		self.assertTrue(utils.send_new_token(subscriber))

	def test_sendmessages_hooks (self):
		self.assertIn('activate_token', sendmessages.hooks('{{ token_url_for_email|/ }}'))
		exception = ''
		try:
			sendmessages.hooks('{{ token_url_for_email||| }}')
		except sendmessages.HookException as e:
			exception = e
		self.assertIn('misconfigured token hook', exception)
		try:
			sendmessages.hooks('{{ purr| }}')
		except sendmessages.HookException as e:
			exception = e
		self.assertIn('misconfigured hook', exception)

	# sendmessages command testing is not viable here and done in tests_func
