from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
	url(r'^$', 'mailer.views.index', name='index'),
	url(r'unsubscribe$', 'mailer.views.unsubscribe', name='unsubscribe'),
	url(r'subscribe$', 'mailer.views.subscribe', name='subscribe'),
	url(r'invited$', 'mailer.views.subscribe_invited', name='subscribe_invited'),
	url(r'confirm_list$', 'mailer.views.confirm_list', name='confirm'),
	url(r'confirm$', 'mailer.views.confirm', name='confirm'),
	url(r'manage$', 'mailer.views.manage', name='manage'),
	url(r'change_token$', 'mailer.views.change_token', name='change token'),
	url(r'generate_tokens$', 'mailer.views.generate_tokens', name='generate tokens'),
)
