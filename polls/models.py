from django.db import models
from jsonfield import JSONField
from tinymce import models as tinymce_models
import uuid

HTML = '<p></p>'


class Poll (models.Model):
	title           = models.CharField(max_length=254)
	text            = tinymce_models.HTMLField(default=HTML)
	date_created    = models.DateTimeField('date created', auto_now=True)
	is_open         = models.BooleanField(default=True)
	show_results    = models.BooleanField(default=True)
	token_required  = models.BooleanField(default=False)
	mode            = models.CharField(max_length=4, choices=(
		("vt", "Simple vote"),
		("sg", "Suggestions"),
	), default="vt")
	uuid            = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

	def __unicode__ (self):
		return self.title


class Option (models.Model):
	poll            = models.ForeignKey(Poll)
	value           = models.CharField(max_length=254)
	votes_counter   = models.IntegerField(default=0)
	votes_tokens    = JSONField(default=[], blank=True)

	def __unicode__ (self):
		return self.poll.title + ' | ' + self.value


# this is one-time-use token for invite-only polls
class Token (models.Model):
	uuid           = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
	purpose        = models.CharField(max_length=30, default='admin')

	def __unicode__ (self):
		return str(self.uuid)
