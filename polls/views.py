# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils.crypto import get_random_string
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _
from ninamori.utils import require_POST, is_email_access_validated, has_enough_time_passed
from models import Poll, Option, Token
from utils import uuid4hex, get_tokens_from_cookies
from datetime import datetime, timedelta
import uuid
import json
import logging

logger = logging.getLogger(__name__)


def activate_token (request):
	if not request.GET.get('token') or not request.GET.get('redirect_url'):
		return render(request, '404.html', {'message': _('Bad request')}, status=400)
	token_uuid = request.GET.get('token').replace('-', '')
	redirect_url = request.GET.get('redirect_url')
	if not uuid4hex.match(token_uuid) or not Token.objects.filter(uuid=token_uuid):
		return render(request, '403.html', {'message': _('Invalid token')}, status=403)

	result = redirect(redirect_url)
	token_cookie = get_tokens_from_cookies(request.COOKIES)

	if not token_uuid in token_cookie: token_cookie.append(token_uuid)
	result.set_cookie(key='token', value=json.dumps(token_cookie), max_age=3*24*60*60)
	logger.debug('token [%s] activated by [%s]' % (token_uuid, request.META.get('REMOTE_ADDR')))
	return result


def select_token (cookies):
	result = {'token': False, 'leftovers': []}
	tokens = get_tokens_from_cookies(cookies)
	if not len(tokens):
		return result
	for token in tokens:
		if uuid4hex.match(token) and Token.objects.filter(uuid=token):
			result['leftovers'].append(token)
			result['token'] = token
	return result


def remove_token(response, token):
	db_token = Token.objects.get(uuid=token['token'])
	db_token.delete()
	token['leftovers'].remove(token['token'])
	response.set_cookie(key='token', value=json.dumps(token['leftovers']), max_age=3*24*60*60)
	return response


@require_POST
def vote (request):
	if not request.POST.get('uuid'):
		return render(request, '404.html', {'message': _('Missing poll id')}, status=400)
	poll_uuid = request.POST.get('uuid')
	if not uuid4hex.match(poll_uuid) or not Poll.objects.filter(uuid=poll_uuid):
		return render(request, '404.html', {'message': _('Invalid poll id')}, status=400)
	poll = Poll.objects.get(uuid=poll_uuid)
	if poll.token_required:
		token = select_token(request.COOKIES)
		if not token['token']:
			result = render(request, '403.html', {'message': _('You shall not pass')}, status=403)
			result.set_cookie(key='token', value=json.dumps(token['leftovers']), max_age=3*24*60*60)
			return result

	if request.POST.get('option') == 'suggestion' and request.POST.get('suggestion') and poll.mode == 'sg':
		option = Option.objects.create(poll=poll, value=request.POST.get('suggestion'))
	elif not request.POST.get('option') or not request.POST.get('option').isdigit() or not Option.objects.filter(id=request.POST.get('option')):
		return render(request, '404.html', {'message': _('Invalid option id')}, status=400)
	else:
		option = Option.objects.get(id=request.POST.get('option'))

	if option.poll != poll:
		return render(request, '404.html', {'message': _('Option id mismatch')}, status=400)
	option.votes_counter += 1
	logger.info('[%s] voted for [%s] in [%s]' % (request.META.get('REMOTE_ADDR'), option.value, poll.title))
	response = redirect(request.POST.get('back_url') or '/')
	if poll.token_required:
		response = remove_token(response, token)
		option.votes_tokens.append(token['token'])
		logger.debug('token [%s] used and removed' % token['token'])

	option.save()
	return response


# TODO use form for this def, decopypastation pas required
# Sdaly, there is not much to decopypastify unless refactor token generation in general
@require_POST
def generate_tokens (request):
	if not (request.user.has_perm('accounts.polls_generate_tokens') or request.user.is_staff):
		return render(request, '403.html', {'message': _('You shall not pass')}, status=403)

	time = has_enough_time_passed(request.user.userprofile.polls_tokens_last_generated, timedelta(hours=1))
	if not time:
		request.session['accounts_admin_index_message'] = _('Try again later')
		return redirect(reverse('accounts.views.admin_index'))

	token_count = request.POST.get('token_count')
	purpose = 'Web request by ' + request.user.username
	email = request.user.email
	qr = request.POST.get('qr')
	qr6 = (qr == '6')
	qr24 = (qr == '24')
	poll_redirect = request.POST.get('redirect')

	# TODO unhardcode such strict limitation as you refactor this thing to be async
	if not (token_count and token_count >= 48):
		return render(request, '403.html', {'message': _('Incorrect token count')}, status=400)

	if not is_email_access_validated(email):
		return render(request, '403.html', {'message': _('You should validate your email address first')}, status=403)

	if not poll_redirect:
		return render(request, '403.html', {'message': _('Redirect is missing')}, status=400)

	if qr and not (qr == '1' or qr6 or qr24):
		return render(request, '403.html', {'message': _('Incorrect qr per page value')}, status=400)

	request.user.userprofile.polls_tokens_last_generated = time
	request.user.userprofile.save()
	call_command('generate_polls_tokens', token_count=token_count, redirect=poll_redirect, qr=bool(qr), qr6=qr6, qr24=qr24, email=email, purpose=purpose, supersilent=True)
	request.session['accounts_admin_index_message'] = _('Tokens generated and soon will be sent to you by email')
	return redirect(reverse('accounts.views.admin_index'))
