# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, User, Permission, Group
from django.contrib.sessions.middleware import SessionMiddleware
from django.test import TestCase, RequestFactory
from models import UserProfile
from mailer.models import List
from ninamori.utils import email_access_validate, is_email_access_validated
from templatetags import accounts_tags
import os
import utils
import views
import urls

class AccountsTests (TestCase):
	def setUp (self):
		self.factory = RequestFactory()
		self.session = SessionMiddleware()
		self.disabled_features = settings.DISABLED_FEATURES
		settings.DISABLED_FEATURES = []

	def tearDown(self):
		settings.DISABLED_FEATURES = self.disabled_features

	def test_models_userprofile (self):
		user = User.objects.create(username='meow')
		userprofile = user.userprofile
		self.assertFalse(userprofile.validation_email_valid_admin())
		self.assertEqual(type(userprofile.__str__()), unicode)

	def test_templatetag_get_avatar (self):
		utils.download_gravatar_if_updated(('goury@slayers.ru', 'x', 'monsterid'), True)
		self.assertEqual(accounts_tags.get_avatar('anonymous@localhost', settings.GRAVATAR_SIZES[0]), '/static/img/anonymous.png')
		self.assertEqual(type(accounts_tags.get_avatar('goury@slayers.ru', settings.GRAVATAR_SIZES[0])), unicode)

	def test_urls (self):
		settings.DISABLED_FEATURES = ['accounts']
		self.assertTrue(reload(urls))
		settings.DISABLED_FEATURES = []
		self.assertTrue(reload(urls))

	def test_utils_adjust_comments_next_notification_date (self):
		user = User.objects.create(username='meow')
		userprofile = user.userprofile
		date1 = userprofile.comments_next_notification_date
		utils.adjust_comments_next_notification_date(userprofile, timedelta(days=1))
		date2 = userprofile.comments_next_notification_date
		self.assertEqual(date2-date1, timedelta(days=1))

	def test_utils_api_register (self):
		self.assertEqual(utils.api_register('meow', 'm@e.ow', 'purr')['status'], 'ok')     # everythong is file
		self.assertEqual(utils.api_register('meow', 'm@e.ow', 'purr')['status'], 'error')  # user exists
		self.assertEqual(utils.api_register('purr', 'me@oww', 'purr')['status'], 'error')  # invalid email
		self.assertEqual(utils.api_register('purr', 'm@e.ow', 'purr')['status'], 'error')  # email already used

	def test_utils_make_and_send_validation_code (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		self.assertFalse(user.userprofile.validation_email_sent)
		utils.make_and_send_validation_code(user)
		self.assertTrue(user.userprofile.validation_email_sent)
		email_access_validate(user.email)
		utils.make_and_send_validation_code(user)
		self.assertTrue(user.userprofile.validation_email_sent)

	def test_utils_make_and_send_authorization_key (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		self.assertFalse(user.userprofile.authorization_key)
		utils.make_and_send_authorization_key(user)
		self.assertTrue(user.userprofile.authorization_key)

	def test_utils_make_and_send_restoration_key (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		self.assertFalse(user.userprofile.restore_code)
		utils.make_and_send_restoration_key(user)
		self.assertTrue(user.userprofile.restore_code)

	def test_utils_download_gravatar_if_updated (self):
		email = 'goury@slayers.ru'
		email_hash = utils.get_gravatar_hash(email)
		path = utils.get_gravatar_file_path(email_hash, settings.GRAVATAR_SIZES[0])
		try:
			os.remove(path)
		except OSError:
			pass
		avatar = utils.download_gravatar_if_updated((email, 'x', 'monsterid'), True)
		self.assertEqual(avatar[0], (path, True))
		avatar = utils.download_gravatar_if_updated((email, 'x', 'monsterid'), True)
		self.assertEqual(avatar[0], (path, False))

	def test_utils_restore (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		self.assertFalse(utils.restore('me', None, None, 60))      # username not found
		self.assertFalse(utils.restore(None, 'ow', None, 60))      # email not found
		self.assertFalse(utils.restore('meow', 'purr', None, 60))  # email do not match
		self.assertTrue(utils.restore('meow', None, None, 60))     # good request
		self.assertFalse(utils.restore('meow', None, None, 60))    # too soon to make another request

	def test_utils_veryfy_code (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		restore_code = utils.restore('meow', None, None, 60)['code']
		user.userprofile.refresh_from_db()
		self.assertFalse(utils.veryfy_code('meow', str(user.id), 60))                        # wrong code
		self.assertFalse(utils.veryfy_code(restore_code, '65535', 60))                       # wrong id
		self.assertFalse(utils.veryfy_code(restore_code, '65535', -1))                       # too late
		self.assertTrue(utils.veryfy_code(restore_code, str(user.id), 60))                   # good code

	def test_views_is_new_key_available (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		self.assertEqual(type(views.is_new_key_available(user.userprofile)), bool)

	def test_views_get_data_for_index (self):
		request = self.factory.get('/')
		request.user = AnonymousUser()
		user = User.objects.create(username='meow', email='m@e.ow')
		self.assertEqual(type(views.get_data_for_index(user)), dict)
		self.assertEqual(type(views.get_data_for_index(request.user)), dict)

	def test_views_index (self):
		request = self.factory.get('/?code=meow')
		self.session.process_request(request)
		request.session['account_updated'] = True
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.assertTrue(views.index(request))

	def test_views_admin_index (self):
		List.objects.create(name='meow')
		Group.objects.create(name='meow')
		request = self.factory.get('/')
		request.user = User.objects.create(username='meow')
		request.user.user_permissions.add(Permission.objects.get(codename='create_user_with_key'))
		request.user.user_permissions.add(Permission.objects.get(codename='mailer_generate_tokens'))
		request.user.user_permissions.add(Permission.objects.get(codename='polls_generate_tokens'))
		request.user.user_permissions.add(Permission.objects.get(codename='create_user_with_key'))
		self.session.process_request(request)
		request.LANGUAGE_CODE = 'en'
		self.assertTrue(views.admin_index(request))

	def test_views_web_create_user_by_admin (self):
		group = Group.objects.create(name='meow')
		user = User.objects.create(username='meow')
		request = self.factory.get('/')
		request.user = user
		request.LANGUAGE_CODE = 'en'
		self.session.process_request(request)
		self.assertEqual(views.web_create_user_by_admin(request).status_code, 405)  # POST request excepted
		request.method = 'POST'
		self.assertEqual(views.web_create_user_by_admin(request).status_code, 403)  # permission is required
		request.user.user_permissions.add(Permission.objects.get(codename='create_user_with_key'))
		request.user = User.objects.get(username='meow')
		self.assertEqual(views.web_create_user_by_admin(request).status_code, 302)  # form is invalid
		request.POST = {'username': 'meow', 'group': '9001'}
		self.assertEqual(views.web_create_user_by_admin(request).status_code, 400)  # Group.DoesNotExist
		request.POST = {'username': 'meow', 'group': str(group.id)}
		self.assertEqual(views.web_create_user_by_admin(request).status_code, 400)  # fsername is not unique
		request.POST = {'username': 'purr', 'group': str(group.id)}
		self.assertEqual(views.web_create_user_by_admin(request).status_code, 302)  # user created
		request.method = 'GET'
		self.assertEqual(views.admin_index(request).status_code, 200)               # cover message retrieving

	def test_views_login (self):
		user = User.objects.create_user(username='meow', email='m@e.ow', password='purr')
		request = self.factory.get('/')
		self.session.process_request(request)
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.login(request).status_code, 405)  # POST request expected
		request = self.factory.post('/', {'username': 'meow', 'password': 'purr'})
		self.session.process_request(request)
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.login(request).status_code, 302)  # good login

	def test_views_logout (self):
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.logout(request).status_code, 405)  # POST request expected
		request = self.factory.post('/')
		self.session.process_request(request)
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.logout(request).status_code, 400)  # no logout fot not logged in one
		request.user = User.objects.create(username='meow')
		self.assertEqual(views.logout(request).status_code, 302)  # good logout

	def test_views_web_key_authenticate (self):
		request = self.factory.post('/', {})
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.web_key_authenticate(request).status_code, 400)  # key is missing
		request = self.factory.post('/', {'key': 'meow'})
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.web_key_authenticate(request).status_code, 403)  # key is invalid
		user = User.objects.create(username='meow', email='m@e.ow')
		authorization_key = utils.make_and_send_authorization_key(user)
		user.userprofile.refresh_from_db()
		self.session.process_request(request)
		request = self.factory.post('/', {'key': authorization_key})
		request.user = AnonymousUser()
		self.session.process_request(request)
		self.assertEqual(views.web_key_authenticate(request).status_code, 302)  # good key

	def test_views_web_key_authenticate_GET (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		authorization_key = utils.make_and_send_authorization_key(user)
		user.userprofile.refresh_from_db()
		request = self.factory.get('/', {})
		request.LANGUAGE_CODE = 'en'
		request.user = AnonymousUser()
		self.session.process_request(request)
		self.assertEqual(views.web_key_authenticate(request, authorization_key).status_code, 302)  # good key

	def test_views_web_register (self):
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.web_register(request).status_code, 405)  # POST request expected
		request = self.factory.post('/', {})
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.web_register(request).status_code, 200)  # settlement form
		request = self.factory.post('/', {'password': 'meow', 'password_confirm': 'purr', 'username': 'meow', 'email': 'm@e.ow'})
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.web_register(request).status_code, 200)  # passwords mismatch
		request = self.factory.post('/', {'password': 'purr', 'password_confirm': 'purr', 'username': 'meow', 'email': 'm@e.ow'})
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.session.process_request(request)
		self.assertEqual(views.web_register(request).status_code, 302)  # successfull settlement
		request = self.factory.post('/', {'password': 'purr', 'password_confirm': 'purr', 'username': 'meow', 'email': 'm@e.ow'})
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.web_register(request).status_code, 200)  # username exists
		request = self.factory.post('/', {'password': 'purr', 'password_confirm': 'purr', 'username': 'meow2', 'email': 'm@e.ow'})
		request.user = AnonymousUser()
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.web_register(request).status_code, 200)  # email already used

	def test_views_validate_email (self):
		user = User.objects.create(username='meow', email='m@e.ow')
		request = self.factory.post('/', {})
		request.user = user
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.validate_email(request).status_code, 302)  # validation code sent
		self.assertEqual(views.validate_email(request).status_code, 302)  # too soon for another request
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.validate_email(request).status_code, 400)  # code is missing
		request = self.factory.get('/?validation_email_code=meow')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.validate_email(request).status_code, 403)  # bad code
		user.userprofile.refresh_from_db()
		request = self.factory.get('/?validation_email_code=' + user.userprofile.validation_email_code)
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.validate_email(request).status_code, 302)  # good code
		self.assertTrue(is_email_access_validated('m@e.ow'))

	def test_views_check_username (self):
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.check_username(request).status_code, 405)  # username expected
		request = self.factory.get('/?username=meow')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.check_username(request).status_code, 200)  # username not found
		User.objects.create(username='meow')
		self.assertEqual(views.check_username(request).status_code, 200)  # username found

	def test_views_manage (self):
		user = User.objects.create_user(username='meow', password='purr')
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.manage(request).status_code, 405)  # POST request expected
		request = self.factory.post('/', {})
		request.LANGUAGE_CODE = 'en'
		request.user = AnonymousUser()
		self.assertEqual(views.manage(request).status_code, 403)  # authenticated user expected
		request = self.factory.post('/', {})
		request.LANGUAGE_CODE = 'en'
		request.user = user
		self.session.process_request(request)
		self.assertEqual(views.manage(request).status_code, 302)  # blank management form
		request = self.factory.post('/', {'password': 'murr', 'settings_comments_delay_interval': '0'})
		request.LANGUAGE_CODE = 'en'
		request.user = user
		self.session.process_request(request)
		self.assertEqual(views.manage(request).status_code, 302)  # bad password
		request = self.factory.post('/', {'password': 'purr', 'new_password': 'murr', 'new_password_confirm': 'meow', 'settings_comments_delay_interval': '0'})
		request.LANGUAGE_CODE = 'en'
		request.user = user
		self.session.process_request(request)
		self.assertEqual(views.manage(request).status_code, 302)  # new passwords mismatch
		request = self.factory.post('/', {'password': 'purr', 'new_password': 'murr', 'new_password_confirm': 'murr', 'settings_comments_delay_interval': '3'})
		request.LANGUAGE_CODE = 'en'
		request.user = user
		self.session.process_request(request)
		self.assertEqual(views.manage(request).status_code, 302)  # new password set, new delay interval set

	def test_views_web_make_authorization_key (self):
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.user = AnonymousUser()
		self.assertEqual(views.web_make_authorization_key(request).status_code, 405)  # POST request expected
		request = self.factory.post('/')
		request.LANGUAGE_CODE = 'en'
		request.user = AnonymousUser()
		self.assertEqual(views.web_make_authorization_key(request).status_code, 403)  # authorized user expected
		request.user = User.objects.create(username='meow')
		self.session.process_request(request)
		self.assertEqual(views.web_make_authorization_key(request).status_code, 302)  # user email is not validated so no key
		self.assertFalse(request.session.get('accounts_index_message'))
		request.user.email='meow@meow.meow'
		request.user.save()
		email_access_validate('meow@meow.meow')
		self.assertEqual(views.web_make_authorization_key(request).status_code, 302)  # key is made
		self.assertTrue(request.session.get('accounts_index_message'))
		self.assertEqual(views.index(request).status_code, 200)                       # cover message retrieving

	def test_views_web_remove_authorization_key (self):
		user = User.objects.create(username='meow')
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.user = AnonymousUser()
		self.assertEqual(views.web_remove_authorization_key(request).status_code, 403)  # authorized user expected
		request.user = user
		self.assertEqual(views.web_remove_authorization_key(request).status_code, 400)  # no keys to remove
		utils.make_and_send_authorization_key(user)
		self.assertEqual(views.web_remove_authorization_key(request).status_code, 200)  # key removal confirmation
		request = self.factory.post('/')
		request.LANGUAGE_CODE = 'en'
		request.user = user
		self.assertEqual(views.web_remove_authorization_key(request).status_code, 302)  # key removed

	def test_views_web_restore (self):
		user = User.objects.create(username='meow')
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.user = user
		self.assertEqual(views.web_restore(request).status_code, 400)  # should not restore what is not lost
		request.user = AnonymousUser()
		self.session.process_request(request)
		self.assertEqual(views.web_restore(request).status_code, 200)  # begin restoration
		request.session['accounts_restore_requested'] = (datetime.now() - timedelta(seconds=views.ACCOUNT_RESTORATION_DELAY + 60)).strftime(views.TIME_FORMAT)
		self.assertEqual(views.web_restore(request).status_code, 200)  # ready for another attempt

	def test_views_restore_request (self):
		# yup, this one is sure long one
		user = User.objects.create(username='meow', email='m@e.ow')
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		request.GET = {'step': '1'}
		self.session.process_request(request)
		self.assertEqual(views.restore_request(request).status_code, 405)  # POST request expected
		self.assertFalse(request.session.get('accounts_restore_requested'))
		request.method = 'POST'
		self.assertFalse(request.session.get('accounts_restore_requested'))
		request.POST = {'email': 'm.e.o.w'}
		self.assertEqual(views.restore_request(request).status_code, 302)  # bad email
		self.assertFalse(request.session.get('accounts_restore_requested'))
		### no need to test inaccessible parts of code
		# request.POST = {}
		# self.assertEqual(views.restore_request(request).status_code, 302)  # no data provided
		# self.assertFalse(request.session.get('accounts_restore_requested'))
		request.POST = {'username': 'meow'}
		self.assertEqual(views.restore_request(request).status_code, 302)  # ready for step 2
		self.assertTrue(request.session.get('accounts_restore_requested'))
		request.GET = {'step': '2'}
		request.POST = {}
		self.assertEqual(views.restore_request(request).status_code, 302)  # missing code
		self.assertFalse(request.session.get('accounts_restore_approved'))
		request.POST = {'code': 'meow'}
		self.assertEqual(views.restore_request(request).status_code, 302)  # bad code
		self.assertFalse(request.session.get('accounts_restore_approved'))
		user.userprofile.restore_request_made = None
		user.userprofile.save()
		restore_code = utils.restore('meow', None, None, 60)['code']
		request.POST = {'code': restore_code}
		self.assertEqual(views.restore_request(request).status_code, 302)  # good code, ready for step 3
		self.assertTrue(request.session.get('accounts_restore_approved'))
		self.assertEqual(views.restore_request(request).status_code, 400)  # doing it wrong
		self.assertTrue(request.session.get('accounts_restore_approved'))
		request.GET = {'step': '3'}
		request.POST = {}
		self.assertEqual(views.restore_request(request).status_code, 302)  # missing passwords
		self.assertTrue(request.session.get('accounts_restore_approved'))
		request.POST = {'password': 'meow', 'password_confirm': 'purr'}
		self.assertEqual(views.restore_request(request).status_code, 302)  # passwords mismatch
		self.assertTrue(request.session.get('accounts_restore_approved'))
		request.POST = {'password': 'meow', 'password_confirm': 'meow'}
		self.assertEqual(views.restore_request(request).status_code, 302)  # new password set
		self.assertFalse(request.session.get('accounts_restore_requested'))
		self.assertFalse(request.session.get('accounts_restore_approved'))
		request.POST = {'password': 'meow', 'password_confirm': 'meow'}
		self.assertEqual(views.restore_request(request).status_code, 403)  # no hacking
		request.GET = {'step': '2'}
		request.POST = {'code': restore_code}
		self.assertEqual(views.restore_request(request).status_code, 403)  # no hacking
		request.session['accounts_restore_requested'] = (datetime.now() - timedelta(seconds=views.ACCOUNT_RESTORATION_DELAY + 60)).strftime(views.TIME_FORMAT)
		self.assertEqual(views.restore_request(request).status_code, 400)  # too late
		request.GET = {'step': '3'}
		request.session['accounts_restore_approved'] = (datetime.now() - timedelta(seconds=views.ACCOUNT_RESTORATION_DELAY + 60)).strftime(views.TIME_FORMAT)
		self.assertEqual(views.restore_request(request).status_code, 400)  # too late

	def test_views_disabled (self):
		request = self.factory.get('/')
		request.LANGUAGE_CODE = 'en'
		self.assertEqual(views.disabled(request).status_code, 403)  # application is disabled
