# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from csscompressor import compress
from htmlmin import minify
from jsmin import jsmin
from django import template
from django.conf import settings
from ninamori.utils import cache_add, cache_get, get_template_content, render_from_string
from django.template.loader import render_to_string

register = template.Library()
cache_settings = settings.CACHES['default']


# TODO decopypastation pass required
@register.simple_tag
def minify_include_css (path, variables={}):
	try:
		minified_template_code = cache_get('minified_css_'+path)
		assert minified_template_code
	except AssertionError:
		template_code = get_template_content(path)
		minified_template_code = compress(template_code, preserve_exclamation_comments=False)
		minified_template_code = minified_template_code.replace('{{', ' {{').replace('}}', '}} ')
		cache_add('minified_css_'+path, minified_template_code, cache_settings['TIMEOUT'])

	# TODO find solution for compartability
	# sometimes this thing will fail to render some nasty constructions like {{%if a} b {%endif%}}
	# meanwhile do suffer and rewrite your CSS with simpler constructions
	return render_from_string(minified_template_code, variables)


@register.simple_tag
def minify_include_js (path, variables={}):
	try:
		minified_template_code = cache_get('minified_js_'+path)
		assert minified_template_code
	except AssertionError:
		template_code = get_template_content(path)
		minified_template_code = jsmin(template_code)
		minified_template_code = minified_template_code.decode('utf8').replace('{%include', '{%include ').replace('{%minify_include_js', '{%minify_include_js ')
		cache_add('minified_js_'+path, minified_template_code, cache_settings['TIMEOUT'])

	return render_from_string(minified_template_code, variables)


@register.simple_tag
def minify_include (path, variables={}):
	try:
		minified_template_code = cache_get('minified_'+path)
		assert minified_template_code
	except AssertionError:
		template_code = get_template_content(path)
		minified_template_code = minify(template_code.decode('utf-8'))
		cache_add('minified_'+path, minified_template_code, cache_settings['TIMEOUT'])

	return render_from_string(minified_template_code, variables)
