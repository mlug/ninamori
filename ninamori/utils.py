# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.validators import validate_email as django_validate_email
from django.shortcuts import render
from django.template import Context, Template
from django.utils import timezone
from django.utils.decorators import available_attrs
from django.utils.translation import ugettext as _
from functools import wraps
from accounts.models import UserProfile
from mailer.models import Subscriber
import json
import logging
import urllib
from os import devnull, makedirs
from subprocess import call

NULL = open(devnull, 'w')
cache_settings = settings.CACHES['default']
logger = logging.getLogger(__name__)


def validate_email (email):
	try:
		django_validate_email(email)
		return True
	except ValidationError:
		return False


def is_email_access_validated (email):
	if not validate_email(email):
		return False
	subscriber_filter = Subscriber.objects.only('email', 'email_validated').filter(email=email)
	if subscriber_filter and subscriber_filter[0].email_validated:
		return True
	user_filter = User.objects.only('email', 'userprofile').filter(email=email)
	if user_filter and user_filter[0].userprofile.validation_email_valid:
		return True
	return False


def email_access_validate (email):
	subscriber_filter = Subscriber.objects.only('email', 'email_validated').filter(email=email)
	if subscriber_filter and not subscriber_filter[0].email_validated:
		subscriber = subscriber_filter[0]
		subscriber.email_validated = True
		subscriber.save()
	user_filter = User.objects.only('email', 'userprofile').filter(email=email)
	if user_filter and not user_filter[0].userprofile.validation_email_valid:
		profile = user_filter[0].userprofile
		profile.validation_email_valid = True
		profile.save()


def get_name (user):
	if not user.is_authenticated():
		return _('Anonymous')
	if user.first_name and user.last_name:
		return user.first_name+' '+user.last_name
	if user.first_name:
		return user.first_name
	if user.last_name:
		return user.last_name
	return user.username


def update_all_users (update):
	usercount = 0
	updatecount = 0
	for user in User.objects.all():
		usercount += 1
		try:
			user.userprofile
		except UserProfile.DoesNotExist:
			updatecount += 1
			if update:
				UserProfile.objects.create(user=user)
	return (usercount, updatecount)


def download_file (url, file_path):
	try:
		cache_file = urllib.URLopener()
		cache_file.retrieve(url, file_path)
		cache_file.close()
	except IOError as e:
		logger.warning('I/O error({0}): {1}'.format(e.errno, e.strerror))


def mkdir (dir_name):
	try:
		makedirs(dir_name)
	except OSError as e:
		if e.errno == 17:
			pass


def cache_add (key, data, timeout):
	cache.add(cache_settings['PREFIX'] + key, data, timeout)


def cache_get (key):
	return cache.get(cache_settings['PREFIX'] + key)


def cache_delete (key):
	cache.delete(cache_settings['PREFIX'] + key)


def get_template_content (template_name):
	template_content = False
	for template_settings in settings.TEMPLATES:
		for path in template_settings['DIRS']:
			try:
				template_content = open(path+'/'+template_name, 'r').read()
			except IOError:
				pass
	return template_content


def render_from_string (string, data):
	template = Template(string)
	context = Context(data)
	return template.render(context)


def get_last_restart (request):
	return settings.LAST_RESTART


def has_enough_time_passed (time_checked, time_delta):
	if (not time_checked) or (time_checked < timezone.now() - time_delta):
		return timezone.now()
	else:
		return False


def montage_qrcode (index, url, path):
	call(['qrencode', '-o', path+'/qrcode_'+str(index)+'.png', '-s', '20', url], stdout=NULL)
	call([
		'montage',
		'-label', url,
		path+'/qrcode_'+str(index)+'.png',
		'-font', '/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-C.ttf',
		'-pointsize', '18',
		'-background', 'White',
		'-geometry', '980x1005',
		path+'/qrcode_'+str(index)+'.png'
	])


def montage_qrlist (index, page_size, path):
	montage_call_list = []
	montage_call_list.append('montage')
	for i in xrange(1, page_size+1): montage_call_list.append(path+'/qrcode_'+str(i)+'.png')
	montage_call_list.append('-tile')
	if page_size == 6: montage_call_list.append('2x3')
	if page_size == 24: montage_call_list.append('4x6')
	montage_call_list.append('-geometry')
	montage_call_list.append('+50+50')
	montage_call_list.append(path+'/qr_page_'+str(index)+'.png')
	call(montage_call_list, stdout=NULL)


def require_request_method (method, message=None):
	def decorator(func):
		@wraps(func, assigned=available_attrs(func))
		def inner(request, *args, **kwargs):
			if request.method != method:
				return render(request, '404.html', {'message': message or _('You should not access this url directly')}, status=405)
			return func(request, *args, **kwargs)
		return inner
	return decorator


require_POST = require_request_method('POST')
