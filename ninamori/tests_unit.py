# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth.models import User, AnonymousUser
from django.test import TestCase, RequestFactory
from django.utils import timezone
from django.utils.translation import activate as activate_translation
from django.utils.safestring import SafeText
from django.http import HttpRequest
from datetime import datetime, timedelta
from mailer.models import Subscriber
from cms.models import Page
from templatetags import global_tags
import middleware
import utils
import views


class NinamoriTests (TestCase):
	def test_middleware_ForceDefaultLanguageMiddleware (self):
		self.mw = middleware.ForceDefaultLanguageMiddleware()
		request = HttpRequest()
		request.META['HTTP_ACCEPT_LANGUAGE'] = 'en'
		self.mw.process_request(request)
		self.assertFalse(request.META.has_key('HTTP_ACCEPT_LANGUAGE'))

	def test_middleware_stats (self):
		self.stats = middleware.Stats()
		self.factory = RequestFactory()
		factory_request = self.factory.get('/')
		factory_request.LANGUAGE_CODE = 'en'
		debug = settings.DEBUG
		settings.DEBUG = True
		self.stats.process_request(factory_request)
		self.stats.process_view(factory_request, lambda x: x, [], {})
		settings.DEBUG = False
		self.stats.process_request(factory_request)
		self.stats.process_view(factory_request, lambda x: x, [], {})
		settings.DEBUG = debug

	def test_middleware_XrealIpToRemoteaddr (self):
		request = HttpRequest()
		self.mw = middleware.XrealIpToRemoteaddr()
		request.META['HTTP_X_REAL_IP'] = '66.66.66.66'
		self.mw.process_request(request)
		self.assertEqual(request.META['REMOTE_ADDR'], '66.66.66.66')

	def test_utils_validate_email (self):
		self.assertFalse(utils.validate_email('meow'))
		self.assertFalse(utils.validate_email('me@ow'))
		self.assertFalse(utils.validate_email('me@o.w'))
		self.assertFalse(utils.validate_email('m@e@o.ww'))
		self.assertFalse(utils.validate_email('m.e.ow'))
		self.assertTrue(utils.validate_email('m@e.ow'))
		self.assertTrue(utils.validate_email('meow@meow.meow'))

	def test_utils_is_email_access_validated (self):
		user = User.objects.create_user(username='meow', email='1m@e.ow')
		subscriber = Subscriber.objects.create(email='2m@e.ow')
		self.assertFalse(utils.is_email_access_validated('m.e.o.w'))
		self.assertFalse(utils.is_email_access_validated('1m@e.ow'))
		self.assertFalse(utils.is_email_access_validated('2m@e.ow'))
		utils.email_access_validate('1m@e.ow')
		utils.email_access_validate('2m@e.ow')
		self.assertTrue(utils.is_email_access_validated('1m@e.ow'))
		self.assertTrue(utils.is_email_access_validated('2m@e.ow'))

	def test_utils_get_name (self):
		activate_translation('en')
		self.assertEqual('Anonymous', utils.get_name(AnonymousUser()))
		self.assertEqual('Meowe Purrmurr', utils.get_name(User.objects.create_user(username='meow1', first_name='Meowe', last_name='Purrmurr')))
		self.assertEqual('Meowe', utils.get_name(User.objects.create_user(username='meow2', first_name='Meowe')))
		self.assertEqual('Purrmurr', utils.get_name(User.objects.create_user(username='meow3', last_name='Purrmurr')))
		self.assertEqual('meow4', utils.get_name(User.objects.create_user(username='meow4')))

	def test_utils_update_all_users (self):
		user = User.objects.create_user(username='meow')
		user.userprofile.delete()
		utils.update_all_users(True)
		self.assertTrue(hasattr(user, 'userprofile'))

	def test_utils_get_template_content (self):
		self.assertTrue(utils.get_template_content('404.html'))
		self.assertFalse(utils.get_template_content('meow.purr.murr'))

	def test_utils_get_last_restart (self):
		request = HttpRequest()
		self.assertEqual(type(utils.get_last_restart(request)), datetime)

	def test_templatetags_minify_include (self):
		user = User.objects.create_user(username='meow')
		Page.objects.create(alias='index', is_published = True, author=user, content_en='{{ comments }}')
		self.assertEqual(type(global_tags.minify_include('license.txt')), SafeText)

	def test_utils_has_enough_time_passed (self):
		three_more_than_one = utils.has_enough_time_passed(timezone.now() - timedelta(days=3), timedelta(days=1))
		one_less_than_three = utils.has_enough_time_passed(timezone.now() - timedelta(days=1), timedelta(days=3))
		self.assertTrue(three_more_than_one)
		self.assertFalse(one_less_than_three)
		self.assertEqual(type(three_more_than_one), datetime)
		self.assertEqual(type(one_less_than_three), bool)
