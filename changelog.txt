Changes between 0.5.1 and 0.5.2
------------------------------------

* Fix using deprecated get_commentator_name resulting in comments notification failure


Changes between 0.5 and 0.5.1
------------------------------------

* Fix HTTP error handleing for file downloads


Changes between 0.4 and 0.5
------------------------------------

Accounts application:
 * Authorization key now can be issued for use instead of login and password
 * Accounts application can now be disabled
 * Added staff area for staff related requests

Comments application:
 * Max displayed level for tree mode is now limited
 * Avatars is hidden for deep levels (depends on display width)
 * Moderation buttons is now toggleable
 * Fix any anonymous can edit other anonymous comments
 * AJAX branch folding for three mode comments

CMS application:
 * Leaflet maps initial zoom level is now customizable
 * Leaflet maps now allowed to be zoomed in up to level 20
 * Updated leaflet.js to 0.7.7
 * Added link for page editing for logged in staff
 * Private pages with group restrictions

Mailer application:
 * Now using application name instead of site name in HTML message footer
 * Messages can now be test-sent to given email
 * Tokens can now be requested from staff area and will be delivered to email

Polls application:
 * Fix unable to edit token-restricted polls
 * Fix showing vote button for misconfigured polls
 * Tokens can now be requested from staff area and will be delivered to email

Templates:
 * Now templates are based on pure.css
 * Fix footer for HTML message is untranslatable
 * Polls options is now marked as required

Organizational:
 * Fix incorrect parsing css variables (workaround: forced adding whitespace around to prevent merging with other parameters)
 * Using StripWhitespaceMiddleware instead of django-compresshtml now
 * Sending 'Cache-Control: no-cache, must-revalidate, max-age=0' header by default for browsers to use cache
 * Added stiemap.xml view with every public non-restricted CMS page
 * JS code marked as AGPL v3 according to LibreJS
 * Sensitive security keys now stored in database as hashes instead of actual keys
 * JS and CSS files rendered as separate files so they can be cached by browser separately
 * More options for staff to mess with users profiles
 * Language switcher can now be disabled
 * Fix robots.txt incorrect mime type
 * Now using django-mail-queue application instead of just Django for sending emails
 * Applications cookies is now set to have max_age of three days
 * Site icon no longer used as default avatar for anonymous users

Changes between 0.3 and 0.4:
------------------------------------

Accounts application:
 * Restored from old project
 * Email validation is centralized thus there is no need to validate address if its already subscribed to mailer and mailer management page accessed once
 * Application can now be disabled
 * Gravatars is now cached on server for security reasons, schedule management command to download updated gravatars
 * Authentication by authorization keys
 * User can change his password
 * User can reset lost password by email authentication

API application:
 * Yandex Money feedback

CMS application:
 * Fix wrong mlug meeting date calculation again
 * Leaflet maps can now be created and used on pages
 * Author for new page is set to current staff user by default
 * Private pages accessible via group membership with key authentication form for anonymous users

Comments application:
 * Initial implementation

Feedback application:
 * Feedback messages now uses reply-to header
 * Authenticated users now use their accounts email address
 * Username and links to admin panel pages sent for authenticated user feedback
 * Email confirmation status is sent too

Mailer application:
 * Email address is marked as validated as soon as management page is accessed by subscriber

Templates:
 * Link to admin panel in footer for authenticated staff members
 * Many minor beautifications

Organizational:
 * Made possible to disable some features
 * Moved INSTALLED_APPS form settings to ninamori_settings
 * Using caches from now on, memcached and python-memcached is requirements
 * Ninamori require pythonutils now
 * Ninamori also requires csscompressor, csscompressor and jsmin now
 * Fix site names dictionary rendered instead of just site name for new session
 * CSS and JS output is minified and cached now
 * Using localizable date filter from django.template instead of locale.setlocale from now on

Translations:
 * Fix tons of typos


Changes between 0.2 and 0.3:
------------------------------------

API application:
 * Stub

CMS application:
 * Fix wrong mlug meeting date calculation
 * Support translatable content
 * Added hook for feedback form
 * Added custom links functiontionality
 * Pages and links now can be ordered manually

Feedback application:
 * Initial implementation

Locking application:
 * Updated and partially rewritten to work with modern Django and Ninamori
 * Integrated with CMS application admin panel

Mailer application:
 * Fix sending double poll tokens
 * Fix unable to manage HTML preference
 * Fix list titles does not use translatable titles
 * Settings moved to global settings
 * Moved messages to templates
 * Messages with empty content for subscriber language will always be sent in default language
 * Messages with empty text or HTML will be forced to be existing type
 * Access token can be changed on management page, new token will be sent to subscribers email
 * Private mailing lists with token invitations
 * Added generate_mailer_tokens command for bulk generation of mailing list invitation tokens
 * Tweaked word wrapping to ensure that URLs (and other long words) will stay together unless its violates RFC2822

Polls application:
 * Renamed command for bulk token generation to generate_polls_tokens
 * Fix bug allowing to suggest options even if poll is not in suggestion mode
 * Used poll tokens now stored in option object

Styles:
 * Theming functionality implemented
 * Static files is no longer in repository
 * Colors now configured in settings
 * Fix incorrect styles for low resolution screens
 * Language selector is now displayed by default
 * Django flat theme updated to 1.1.3

Templates:
 * Templates is now separate repository

Translations:
 * Site name and mailer application name is now translatable via settings

Organizational:
 * Made user manual
 * Replaced readme.txt with readme.md
 * Rebranded project to be called Ninamori CMS
 * HTTP_X_REAL_IP support
 * Updated settings examples
 * Added changelog.txt and version.txt


Changes between 0.1 and 0.2:
------------------------------------

Mailer application:
 * Implemented urgent messages
 * Fix message name encoding error
 * Implemented RFC2822 compatibility

Polls application:
 * It is now possible to activate multiple tokens
 * Template now uses label tags
 * Fix option validation raising errors
 * Fix incorrect style
 * Fix JS parser
 * Polls with suggestions implemented
 * Bulk token and qr code generator implemented

Styles:
 * FIX entire menu tab is not clickable
 * Colors now configured in utils

Templates:
 * Moved code from python files to templates

Translations:
 * Added en and ru locales

Organizational:
 * Added proper requirements.txt
 * Made readme.txt with deployment example


Changes between pre-release and 0.1:
------------------------------------

CMS application:
 * Main menu implemented

Mailer application:
 * Maximum delay is limited to 30 days
 * Implemented private mailing lists

Polls application:
 * Poll results widget for CMS
 * Tokens required for private polls

Styles:
 * Style beautified

Translations:
 * Initial implementation
 * Language switcher implemented (hidden)
