from django.contrib import admin
from models import YandexBalance


class YandexBalanceAdmin (admin.ModelAdmin):
	list_display = ('issue', 'date_created', 'amount_collected')
	fields = ['issue', 'amount_collected']
	ordering = ['-date_created']


admin.site.register(YandexBalance, YandexBalanceAdmin)
