from __future__ import unicode_literals
from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from models import YandexBalance
import hashlib
import logging

logger = logging.getLogger(__name__)


@csrf_exempt
def yandex_inform (request):
	logger.debug('access: ' + str(request.POST.urlencode()))
	try:
		hashtest = unicode(
			request.POST.get('notification_type') +
			'&' + request.POST.get('operation_id') +
			'&' + request.POST.get('amount') +
			'&' + request.POST.get('currency') +
			'&' + request.POST.get('datetime') +
			'&' + request.POST.get('sender') +
			'&' + request.POST.get('codepro') +
			'&' + settings.YANDEX_MONEY_SECRET +
			'&' + request.POST.get('label')
		)
	except TypeError:
		hashtest = HttpResponse("Yay, thanks", content_type="text/plain", status=200)
	if request.POST.get('test_notification') == 'true':
		return HttpResponse("Yay, thanks for testing", content_type="text/plain", status=200)
	if request.POST.get('sha1_hash') == hashlib.sha1(hashtest).hexdigest() and (not request.POST.get('unaccepted') or request.POST.get('unaccepted') == 'false'):
		logger.info('yandex_inform successfull payment for %s RUB (sent by %s %s via %s) [%s]' % (
			request.POST.get('amount'),
			request.POST.get('sender'),
			request.POST.get('email'),
			request.POST.get('notification_type'),
			request.POST.get('operation_id'),
		))
		issue = None
		if request.GET.get('id'):
			try:
				issue = YandexBalance.objects.get(id=request.GET.get('id'))
			except YandexBalance.DoesNotExist:
				logger.warning('requested isue (id=%s) does not exist' % request.GET.get('id'))
		if not issue:
			issue = YandexBalance.objects.latest('date_created')
		issue.amount_collected = unicode(str(float(issue.amount_collected) + float(request.POST.get('amount'))))
		issue.save()
		return HttpResponse("Yay, thanks", content_type="text/plain", status=200)
	return HttpResponse("What?", content_type="text/plain", status=403)
