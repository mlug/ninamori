from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
	url(r'^/(?P<alias>.+)/scripts.js$', 'cms.views.scripts_for_page_by_alias', name='scripts for cms page by alias'),
	url(r'^/(?P<alias>.+)$', 'cms.views.page_by_alias', name='cms page by alias'),
	url(r'^/$', 'cms.views.page_by_alias', {'alias': ''}),
	url(r'^$', 'cms.views.page_by_alias', {'alias': ''}),
)
