# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse, HttpResponseNotAllowed
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django.views.decorators.http import last_modified
from api.models import YandexBalance
from comments.utils import get_top_level_comments_html
from feedback.utils import get_feedback_form_html
from mailer.utils import get_subsrcibe_form_html
from polls.utils import get_poll_form_html, get_poll_results_html
from models import Page
from utils import get_next_2nd_friday_or_last_saturday, get_hooks, get_leaflet_map, get_page_last_modified
import logging

logger = logging.getLogger(__name__)


def hooks (hook, request):
	if hook == 'next_2nd_friday_or_last_saturday':
		return get_next_2nd_friday_or_last_saturday(request.LANGUAGE_CODE, 'l, j E Y')
	if hook == 'yandex_money_form':
		issue = YandexBalance.objects.latest('date_created')
		return render_to_string('cms/hook_yandex_money.html', {
			'protocol': settings.PROTOCOL_SCHEME,
			'issue': issue.issue,
			'amount_collected': issue.amount_collected,
			'site_name': settings.SITE_NAME[request.LANGUAGE_CODE],
			'site_domain': settings.SITE_DOMAIN,
			'receiver': settings.YANDEX_MONEY_ID,
		})
	if hook == 'subscribe_form':
		return get_subsrcibe_form_html(request)
	if 'poll_form|' in hook:
		if hook.count('|') != 2:
			return 'misconfigured pool form hook'
		data = hook.split('|')
		return get_poll_form_html(request, data[1].replace('-', ''), data[2])
	if 'poll_results|' in hook:
		if hook.count('|') != 1:
			return 'misconfigured pool results hook'
		data = hook.split('|')
		return get_poll_results_html(data[1].replace('-', ''))
	if 'leaflet_map|' in hook:
		if hook.count('|') != 1:
			return 'misconfigured leaflet map hook'
		data = hook.split('|')
		return get_leaflet_map(data[1])
	if 'feedback_form' in hook:
		return get_feedback_form_html(request)
	if 'comments' in hook:
		return get_top_level_comments_html(request)

	logger.warning('bad hook [%s] at [%s]' % (str(hook), request.path))
	return str(hook)


def index (request):
	return page_by_alias(request, alias='index')


def page_by_alias (request, alias):
	logger.info('[%s] requested page %s' % (request.META.get('REMOTE_ADDR'), alias))
	if alias == '' or request.path == '/cms/index':
		return redirect("/")
	page = get_object_or_404(Page, alias=alias)
	if not page.is_published:
		raise Http404(_('You shall not pass'))
	allowed_groups = page.allowed_groups.all()
	if allowed_groups:
		if not set(request.user.groups.all()) & set(allowed_groups):
			url_key_authenticate = reverse('accounts.views.web_key_authenticate') if request.GET.get('key_auth') else None
			return render(request, 'cms/private_page.html', {
				'title': page.localized_title(request.LANGUAGE_CODE),
				'url': reverse(page_by_alias) + '/' + alias,
				'url_key_authenticate': url_key_authenticate,
			})
	header_includes = page.header_includes.split('\r\n') if page.header_includes else []
	footer_includes = page.footer_includes.split('\r\n') if page.footer_includes else []
	content = page.localized_content(request.LANGUAGE_CODE)
	scripts = []
	for hook in get_hooks(content):
		hook_name = hook.replace('{{ ', '').replace(' }}', '').replace('{{', '').replace('}}', '')
		hook_render = hooks(hook_name, request)
		if type(hook_render) == tuple:
			scripts.append(hook_render[1])
			hook_render = hook_render[0]
		content = content.replace(hook, hook_render)
	return render(request, 'cms/page.html', {
		'title': page.localized_title(request.LANGUAGE_CODE),
		'url': reverse(page_by_alias) + '/' + alias,
		'url_edit_page': reverse('admin:cms_page_change', args=(page.id,)),
		'content': content,
		'header_includes': header_includes,
		'footer_includes': footer_includes,
		'scripts': scripts,
	})


@last_modified(get_page_last_modified)
def scripts_for_page_by_alias (request, alias):
	page = get_object_or_404(Page, alias=alias)
	if not page.is_published:
		raise Http404(_('You shall not pass'))
	content = page.localized_content(request.LANGUAGE_CODE)
	scripts = []
	for hook in get_hooks(content):
		hook_name = hook.replace('{{ ', '').replace(' }}', '').replace('{{', '').replace('}}', '')
		hook_render = hooks(hook_name, request)
		if type(hook_render) == tuple:
			scripts.append(hook_render[1])
	return render(request, 'cms/scripts.js', {'scripts': scripts}, content_type='application/javascript')
