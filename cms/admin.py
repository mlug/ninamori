# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.html import escape
from django.utils.translation import ugettext as _
from locking.admin import LockableAdmin
from locking.forms import LockingForm
from models import Page, MenuLink, LeafletMap

action_names = {
		ADDITION: 'Addition',
		CHANGE:	 'Change',
		DELETION: 'Deletion',
}


class PageAdmin (LockableAdmin):
	form = LockingForm
	list_display = ('list_title', 'alias', 'order', 'get_lock_for_admin', 'date_created', 'author', 'is_published', 'is_in_menu')
	list_editable = ('is_published', 'is_in_menu', 'order')
	fieldsets = [
		(None,           {'fields': ['title_en', 'title_ru', 'alias', ('content_en', 'content_ru'), ('header_includes', 'footer_includes', 'allowed_groups'), ('author', 'is_published', 'is_in_menu')]}),
	]
	ordering = ['order']
	list_filter = (
		('is_published', admin.BooleanFieldListFilter),
	)

	def formfield_for_foreignkey (self, db_field, request, **kwargs):
		if db_field.name == 'author':
			kwargs['initial'] = request.user.id
		return super(PageAdmin, self).formfield_for_foreignkey(
			db_field, request, **kwargs
		)

	def view_on_site(self, obj):
		return reverse('cms.views.page_by_alias') + '/' + obj.alias


class MenuLinkAdmin (admin.ModelAdmin):
	list_display = ('title_en', 'order', 'url')
	list_editable = ('order',)
	ordering = ['order']


class LeafletMapAdmin (admin.ModelAdmin):
	list_display = ('title',)


admin.site.index_title = _('Admin control unit')
admin.site.site_title = settings.SITE_NAME[settings.LANGUAGE_CODE]
admin.site.site_header = settings.SITE_NAME[settings.LANGUAGE_CODE]
admin.site.register(Page, PageAdmin)
admin.site.register(MenuLink, MenuLinkAdmin)
admin.site.register(LeafletMap, LeafletMapAdmin)


# <this part is made mostly by jakub and tgandor from djangosnippets.org>
class FilterBase(admin.SimpleListFilter):
		def queryset(self, request, queryset):
				if self.value():
						dictionary = dict(((self.parameter_name, self.value()),))
						return queryset.filter(**dictionary)


class ActionFilter(FilterBase):
		title = 'action'
		parameter_name = 'action_flag'
		def lookups(self, request, model_admin):
				return action_names.items()


class UserFilter(FilterBase):
		"""Use this filter to only show current users, who appear in the log."""
		title = 'user'
		parameter_name = 'user_id'
		def lookups(self, request, model_admin):
				return tuple((u.id, u.username)
						for u in User.objects.filter(pk__in =
								LogEntry.objects.values_list('user_id').distinct())
				)


class AdminFilter(UserFilter):
		"""Use this filter to only show current Superusers."""
		title = 'admin'
		def lookups(self, request, model_admin):
				return tuple((u.id, u.username) for u in User.objects.filter(is_superuser=True))


class StaffFilter(UserFilter):
		"""Use this filter to only show current Staff members."""
		title = 'staff'
		def lookups(self, request, model_admin):
				return tuple((u.id, u.username) for u in User.objects.filter(is_staff=True))


class LogEntryAdmin(admin.ModelAdmin):
		date_hierarchy = 'action_time'
		readonly_fields = LogEntry._meta.get_all_field_names()

		list_filter = [
				UserFilter,
				ActionFilter,
				'content_type',
				# 'user',
		]

		search_fields = [
				'object_repr',
				'change_message'
		]

		list_display = [
				'action_time',
				'user',
				'content_type',
				'object_link',
				'action_flag',
				'action_description',
				'change_message',
		]

		def has_add_permission(self, request):
				return False

		def has_change_permission(self, request, obj=None):
				return request.user.is_superuser and request.method != 'POST'

		def has_delete_permission(self, request, obj=None):
				return False

		def object_link(self, obj):
				ct = obj.content_type
				repr_ = escape(obj.object_repr)
				try:
						href = reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id])
						link = '<a href="%s">%s</a>' % (href, repr_)
				except NoReverseMatch:
						link = repr_
				return link if obj.action_flag != DELETION else repr_
		object_link.allow_tags = True
		object_link.admin_order_field = 'object_repr'
		object_link.short_description = 'object'

		def queryset(self, request):
				return super(LogEntryAdmin, self).queryset(request) \
						.prefetch_related('content_type')

		def action_description(self, obj):
				return action_names[obj.action_flag]
		action_description.short_description = 'Action'


admin.site.register(LogEntry, LogEntryAdmin)
# </this part is made mostly by jakub and tgandor from djangosnippets.org>
